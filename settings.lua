local rawsettings = ...;

local Signal = loadstring(game:HttpGet"https://gitlab.com/TechHog/irisui/-/raw/main/signal.lua")();
local changed_signals = {};
for key in next, rawsettings do
    changed_signals[key] = Signal.new();
end;

local settings = setmetatable({}, {
    __index = rawsettings,
    __newindex = function(self, key, value)
        if type(value) == "nil" then
            return error("cannot set a settings value to nil", 2);
        end;

        local oldvalue = rawsettings[key];
        if value == oldvalue then return; end;
        if type(oldvalue) == "nil" then
            return error(key .. " is not a valid setting key", 2);
        end;

        rawsettings[key] = value;
        changed_signals[key]:Fire(value);
    end
});

return settings, rawsettings, changed_signals;