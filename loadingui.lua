local old = game.CoreGui:FindFirstChild("irisloadingstatusgui");
if old then
    old:Destroy();
    old = nil;
end;

local gui = Instance.new("ScreenGui", game.CoreGui);
gui.Name = "irisloadingstatusgui";

local function destroy()
    gui:Destroy();
end;

local frame = Instance.new("Frame");
frame.Size = UDim2.fromOffset(300, 100);
frame.Position = UDim2.new(0.5, -150, 0.5, -50);

frame.BackgroundColor3 = Color3.fromRGB(20, 20, 20);
frame.BorderColor3 = Color3.fromRGB(200, 200, 200);
frame.BorderSizePixel = 1;

frame.Parent = gui;

local title = Instance.new("TextLabel");
title.Size = UDim2.new(1, -10, 0, 20);
title.Position = UDim2.fromOffset(5, 5);

title.BackgroundTransparency = 1;

title.Text = `{(...)} | made by techhog`;
title.TextColor3 = Color3.fromRGB(200, 200, 200);
title.TextXAlignment = Enum.TextXAlignment.Left;
title.TextYAlignment = Enum.TextYAlignment.Center;

title.Parent = frame;

local subtitle = Instance.new("TextLabel");
subtitle.Size = UDim2.fromScale(1, 1);
subtitle.Position = UDim2.fromOffset(0, title.TextBounds.Y + 2);

subtitle.BackgroundTransparency = 1;

subtitle.Text = "Loading...";
subtitle.TextColor3 = Color3.fromRGB(150, 150, 150);
subtitle.TextXAlignment = Enum.TextXAlignment.Left;
subtitle.TextYAlignment = Enum.TextYAlignment.Center;

subtitle.Parent = title;

local content = Instance.new("TextLabel");
content.Size = UDim2.fromScale(1, 1);
content.Position = UDim2.new(0, 5, 0.125, 0);

content.BackgroundTransparency = 1;

content.Text = "Loading...";
content.TextColor3 = Color3.fromRGB(150, 150, 150);
content.TextXAlignment = Enum.TextXAlignment.Center;
content.TextYAlignment = Enum.TextYAlignment.Center;

content.Parent = frame;
local function update(text)
    content.Text = text;
end;

return {destroy = destroy, update = update};