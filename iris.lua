local rawsettings, uifail, uicallback = ...;

local iris_source = game:HttpGet("https://raw.githubusercontent.com/TechHog8984/Iris/main/bundle.lua"):gsub('game:GetService%("Players"%)%.LocalPlayer:WaitForChild%("PlayerGui"%)', 'game:GetService("CoreGui")');
local iris, iris_err = loadstring(iris_source);

if iris then
    iris, iris_err = iris();
    if iris then
        iris = iris.Init();
    else
        uifail(iris_err);
        return;
    end;
else
    uifail(iris_err);
    return;
end;

local states = {};
for key, value in next, rawsettings do
    states[key] = iris.State(value);
end;

local function newcallback()
    uicallback(states);
end;

-- defer because I initialize some stuff after this function that is used in the callback
task.defer(iris.Connect, iris, newcallback);

return iris, newcallback;