local Connection = {};Connection.__index = Connection;
function Connection.new(signal, callback)
    return signal:_add(setmetatable({
        _signal = signal,
        _callback = callback
    }, Connection));
end;
function Connection:Disconnect()
    self._signal:_remove(self);
end;

local Signal = {};Signal.__index = Signal;

function Signal.new(connectInit)
    return setmetatable({
        _connections = {},
        connectInit = connectInit
    }, Signal);
end;
function Signal:_add(con)
    self._connections[con] = true;
    return con;
end;
function Signal:_remove(con)
    self._connections[con] = nil;
end;

function Signal:Connect(callback)
    local con = Connection.new(self, callback);

    if self.connectInit then
        self.connectInit(con);
    end;

    return con;
end;
function Signal:Once(callback)
    local con; con = self:Connect(function(...)
        con:Disconnect();
        callback(...);
    end);
    return con;
end;
function Signal:Fire(...)
    local args = {...};
    for connection in next, self._connections do
        task.spawn(function()
            xpcall(connection._callback, function(err)
                -- warn("Failed to call connection : " .. err);
                warn(`Failed to call connection : {err}`);
            end, unpack(args));
        end);
    end;
end;

return Signal;
